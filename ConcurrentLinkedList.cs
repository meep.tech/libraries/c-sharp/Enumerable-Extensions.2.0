﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
namespace Meep.Tech.Collections {

  public interface IConcurrentLinkedList<T>: IEnumerable<T> {
    ConcurrentLinkedList.Node<T> First { get; }

    /// <summary>
    /// Attempts to add the specified value to the <see cref="ConcurrentLinkedList{T}"/>.
    /// </summary>
    bool TryAdd(T value);

    /// <summary>
    /// Attempts to remove the specified value from the <see cref="ConcurrentLinkedList{T}"/>.
    /// </summary>
    bool Remove(T value, out T result);

    /// <summary>
    /// Determines whether the <see cref="ConcurrentLinkedList{T}"/> contains the specified key.
    /// </summary>
    bool Contains(T value);
  }

  public abstract class ConcurrentLinkedList {

    /// <summary>
    /// A Linked list of items that can be accessed and removed concurrently
    /// </summary>
    internal enum NodeState {
      INS = 0,
      REM = 1,
      DAT = 2,
      INV = 3
    }

    public class Node<T> {
      public T Value;
      public Node<T> Next;

      private int _state;
      private readonly bool _isDummy;

      internal Node<T> Previous;
      internal int ThreadId;
      internal NodeState State {
        get => (NodeState)_state;
        set => _state = (int)value;
      }

      internal Node() {
        _isDummy = true;
        Value = default(T);
      }

      internal Node(T value, NodeState state, int threadId) {
        Value = value;
        ThreadId = threadId;
        _state = (int)state;
        _isDummy = false;
      }

      internal NodeState AtomicCompareAndExchangeState(NodeState value, NodeState compare) {
        return (NodeState)Interlocked.CompareExchange(ref _state, (int)value, (int)compare);
      }

      internal bool IsDummy() {
        return _isDummy;
      }
    }

    internal class ThreadState<T> {
      public int Phase;
      public bool Pending;
      public Node<T> Node;

      public ThreadState(int phase, bool pending, Node<T> node) {
        Phase = phase;
        Pending = pending;
        Node = node;
      }
    }
  }

  public class ConcurrentLinkedList<T> : ConcurrentLinkedList, IConcurrentLinkedList<T> {
    public Node<T> First => _first;

    private int _counter;
    private Node<T> _first;
    private readonly Node<T> _dummy;
    private readonly ConcurrentDictionary<int, ThreadState<T>> _threads;

    public ConcurrentLinkedList() {
      _counter = 0;
      _dummy = new Node<T>();
      _threads = new ConcurrentDictionary<int, ThreadState<T>>();
      _first = new Node<T>(default(T), NodeState.REM, -1);
    }

    /// <summary>
    /// Attempts to add the specified value to the <see cref="ConcurrentLinkedList{T}"/>.
    /// </summary>
    public bool TryAdd(T value) {
      var node = new Node<T>(value, (int)NodeState.INS, Thread.CurrentThread.ManagedThreadId);

      Enlist(node);
      var insertionResult = HelpInsert(node, value);

      var originalValue = node.AtomicCompareAndExchangeState(insertionResult ? NodeState.DAT : NodeState.INV, NodeState.INS);
      if (originalValue != NodeState.INS) {
        HelpRemove(node, value, out _);
        node.State = NodeState.INV;
      }

      return insertionResult;
    }

    /// <summary>
    /// Attempts to remove the specified value from the <see cref="ConcurrentLinkedList{T}"/>.
    /// </summary>
    public bool Remove(T value, out T result) {
      var node = new Node<T>(value, NodeState.REM, Thread.CurrentThread.ManagedThreadId);

      Enlist(node);
      var removeResult = HelpRemove(node, value, out result);
      node.State = NodeState.INV;
      return removeResult;
    }

    /// <summary>
    /// Determines whether the <see cref="ConcurrentLinkedList{T}"/> contains the specified key.
    /// </summary>
    public bool Contains(T value) {
      var current = _first;
      while (current != null) {
        if (current.Value == null || current.Value.Equals(value)) {
          var state = current.State;
          if (state != NodeState.INV) {
            return state == NodeState.INS || state == NodeState.DAT;
          }
        }

        current = current.Next;
      }

      return false;
    }

    public IEnumerator<T> GetEnumerator() {
      Node<T> currentNode = First;
      while(currentNode != null) {
        yield return currentNode.Value;
        currentNode = currentNode.Next;
      }
    }

    private static bool HelpInsert(Node<T> node, T value) {
      var previous = node;
      var current = previous.Next;
      while (current != null) {
        var state = current.State;
        if (state == NodeState.INV) {
          var successor = current.Next;
          previous.Next = successor;
          current = successor;
        } else if (current.Value != null && !current.Value.Equals(value)) {
          previous = current;
          current = current.Next;
        } else if (state == NodeState.REM) {
          return true;
        } else if (state == NodeState.INS || state == NodeState.DAT) {
          return false;
        }
      }

      return true;
    }

    private static bool HelpRemove(Node<T> node, T value, out T result) {
      result = default(T);
      var previous = node;
      var current = previous.Next;

      while (current != null) {
        var state = current.State;
        if (state == NodeState.INV) {
          var successor = current.Next;
          previous.Next = successor;
          current = successor;
        } else if (current.Value != null && !current.Value.Equals(value)) {
          previous = current;
          current = current.Next;
        } else if (state == NodeState.REM) {
          return false;
        } else if (state == NodeState.INS) {
          var originalValue = current.AtomicCompareAndExchangeState(NodeState.REM, NodeState.INS);
          if (originalValue == NodeState.INS) {
            result = current.Value;
            return true;
          }
        } else if (state == NodeState.DAT) {
          result = current.Value;
          current.State = NodeState.INV;
          return true;
        }
      }

      return false;
    }

    private void Enlist(Node<T> node) {
      var phase = Interlocked.Increment(ref _counter);
      var threadState = new ThreadState<T>(phase, true, node);
      var currentThreadId = Thread.CurrentThread.ManagedThreadId;
      _threads.AddOrUpdate(currentThreadId, threadState, (key, value) => threadState);

      foreach (var threadId in _threads.Keys) {
        HelpEnlist(threadId, phase);
      }

      HelpFinish();
    }

    private void HelpEnlist(int threadId, int phase) {
      while (IsPending(threadId, phase)) {
        var current = _first;
        var previous = current.Previous;
        if (current.Equals(_first)) {
          if (previous == null) {
            if (IsPending(threadId, phase)) {
              var node = _threads[threadId].Node;
              var original = Interlocked.CompareExchange(ref current.Previous, node, null);
              if (original is null) {
                HelpFinish();
                return;
              }
            }
          } else {
            HelpFinish();
          }
        }
      }
    }

    private void HelpFinish() {
      var current = _first;
      var previous = current.Previous;
      if (previous != null && !previous.IsDummy()) {
        var threadId = previous.ThreadId;
        var threadState = _threads[threadId];
        if (current.Equals(_first) && previous.Equals(threadState.Node)) {
          var currentState = _threads[threadId];
          var updatedState = new ThreadState<T>(threadState.Phase, false, threadState.Node);
          _threads.TryUpdate(threadId, updatedState, currentState);
          previous.Next = current;
          Interlocked.CompareExchange(ref _first, previous, current);
          current.Previous = _dummy;
        }
      }
    }

    private bool IsPending(int threadId, int phase) {
      var threadState = _threads[threadId];
      return threadState.Pending && threadState.Phase <= phase;
    }

    IEnumerator IEnumerable.GetEnumerator()
      => GetEnumerator();
  }
}