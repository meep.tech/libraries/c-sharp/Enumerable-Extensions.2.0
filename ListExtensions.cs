﻿using System.Collections.Generic;
using System.Linq;

public static class ListExtensions {

  private static System.Random rng = new System.Random();

  public static IList<T> Shuffle<T>(this IList<T> list) {
    int n = list.Count;
    while (n > 1) {
      n--;
      int k = rng.Next(n + 1);
      T value = list[k];
      list[k] = list[n];
      list[n] = value;
    }

    return list;
  }

  /// <summary>
  /// spread opperators
  /// </summary>
  public static void Deconstruct<T>(this IList<T> list, out T first, out IList<T> rest) {

    first = list.Count > 0 ? list[0] : default(T);
    rest = list.Skip(1).ToList();
  }

  /// <summary>
  /// spread opperators
  /// </summary>
  public static void Deconstruct<T>(this IList<T> list, out T first, out T second, out IList<T> rest) {
    first = list.Count > 0 ? list[0] : default(T);
    second = list.Count > 1 ? list[1] : default(T);
    rest = list.Skip(2).ToList();
  }
}