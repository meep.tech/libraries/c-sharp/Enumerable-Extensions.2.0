﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class EnumerableExtensions {

  public static bool IsNull<T>(this T @obj) where T : class
    => obj == null;

  public static bool IsNotNull<T>(this T @obj) where T : class
    => obj != null;

  public static ICollection<TValue> NewCollectionOrAddToExisting<TValue>(this ICollection<TValue> current, TValue value) {
    if (current == null) {
      current = value.AsEnumerable().ToList();
    } else {
      current.Add(value);
    }

    return current;
  }

  public static ICollection<TValue> AsCollectionWith<TValue>(this TValue first, TValue second)
    => new List<TValue> { first, second };

  /// <summary>
  /// Returns empty if the enumerable is null
  /// </summary>
  public static IEnumerable<T> OrEmptyIfNull<T>(this IEnumerable<T> source) {
    return source ?? Enumerable.Empty<T>();
  }

  /// <summary>
  /// map each where if is true,
  /// </summary>
  public static IEnumerable<R> ForEach<T,R>(this IEnumerable<T> enumeration, Func<T,bool> @if, Func<T, R> @map) {
    foreach(T @value in enumeration) {
      if (@if(value)) {
        yield return @map(value);
      }
    }
  }

  /// <summary>
  /// map each where if is true,
  /// </summary>
  public static IEnumerable<R> SelectWhere<T,R>(this IEnumerable<T> enumeration, Func<T,(bool, R)> selectAndMap) {
    foreach (T @value in enumeration) {
      (bool include, R mappedValue) = selectAndMap(value);
      if (include) {
         yield return mappedValue;
      }
    }
  }

  /// <summary>
  /// select where:
  /// </summary>
  public static IEnumerable<R> Select<T, R>(this IEnumerable<T> source, Func<T, R> select, Func<T, bool> until) {
    foreach (T item in source) {
      if (until(item)) {
        yield return select(item);
      } else break;
    }
  }

  /// <summary>
  /// select where until:
  /// </summary>
  public static IEnumerable<R> Select<T, R>(this IEnumerable<T> source, Func<T, bool> where, Func<T, R> select) {
    foreach (T item in source) {
      if (where(item)) {
        yield return select(item);
      }
    }
  }

  /// <summary>
  /// Select where each is true until null is returned.
  /// </summary>
  public static IEnumerable<R> Select<T,R>(this IEnumerable<T> enumeration, Func<T,(bool?, R)> selectAndMapUntil) {
    foreach (T @value in enumeration) {
      (bool? success, R mappedValue) = selectAndMapUntil(value);
      if (!success.HasValue) {
        break;
      }
      if (success.Value) {
       yield return mappedValue;
      }
    }
  }

  /// <summary>
  /// Select where each is true until null is returned.
  /// </summary>
  public static IEnumerable<R> Select<T,R>(this IEnumerable<T> enumeration, Func<T, int, (bool?, R)> selectAndMapUntil) {
    int index = 0;
    foreach (T @value in enumeration) {
      (bool? success, R mappedValue) = selectAndMapUntil(value, index++);
      if (!success.HasValue) {
        break;
      }
      if (success.Value) {
       yield return mappedValue;
      }
    }
  }

  /// <summary>
  /// Select where each is true until null is returned.
  /// </summary>
  public static R FirstOrDefault<T,R>(this IEnumerable<T> enumeration, Func<T, int, (bool, R)> mapFirst) {
    int index = 0;
    foreach (T @value in enumeration) {
      (bool success, R mappedValue) = mapFirst(value, index++);
      if (success) {
       return mappedValue;
      }
    }

    return default;
  }

  /// <summary>
  /// Select where each is true until null is returned.
  /// </summary>
  public static R FirstOrDefault<T,R>(this IEnumerable<T> enumeration, Func<T, int, (bool?, R)> mapFirst) {
    int index = 0;
    foreach (T @value in enumeration) {
      (bool? success, R mappedValue) = mapFirst(value, index++);
      if (!success.HasValue) {
        break;
      }
      if (success.Value) {
       return mappedValue;
      }
    }

    return default;
  }

  /// <summary>
  /// Select where each is true until null is returned.
  /// </summary>
  public static R First<T,R>(this IEnumerable<T> enumeration, Func<T, int, (bool, R)> mapFirst) {
    int index = 0;
    foreach (T @value in enumeration) {
      (bool? success, R mappedValue) = mapFirst(value, index++);
      if (!success.HasValue) {
        break;
      }
      if (success.Value) {
       return mappedValue;
      }
    }

    throw new Exception($"Could not find a matching item");
  }

  /// <summary>
  /// select where until:
  /// </summary>
  public static IEnumerable<T> Where<T>(this IEnumerable<T> source, Func<T, bool?> whereUntil) {
    foreach (T item in source) {
      bool? success = whereUntil(item);
      if (!success.HasValue) {
        break;
      } else if (success.Value)
        yield return item;
    }
  }

  /// <summary>
  /// map each item with the index provided
  /// </summary>
  public static IEnumerable<R> Select<T, R>(this IEnumerable<T> enumeration, Func<int, T, R> @map) {
    int index = 0;
    foreach (T @value in enumeration) {
      yield return @map(index++, value);
    }
  }

  /// <summary>
  /// do on each where if is true,
  /// </summary>
  public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> @do, Func<T, bool> @if) {
    foreach (T @value in enumeration) {
      if (@if(value)) {
        @do(value);
      }
    }
  }

  /// <summary>
  /// do on each
  /// </summary>
  public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> @do) {
    foreach (T @value in enumeration) {
      @do(value);
    }
  }

  /// <summary>
  /// do on each
  /// </summary>
  public static void ForEach<T>(this IEnumerable<T> enumeration, Action<int, T> @do) {
    int index = 0;
    foreach (T @value in enumeration) {
      @do(index++, value);
    }
  }

  /// <summary>
  /// do on each until if is false
  /// </summary>
  public static void ForEach<T>(this IEnumerable<T> enumeration, Func<T, bool> @doUntil, Func<T, bool> @if) {
    foreach (T @value in enumeration) {
      if (@if(value)) {
        if (!@doUntil(value)) {
          break;
        }
      }
    }
  }

  /// <summary>
  /// If one unordered sequence equals another.
  /// The count needs to be the same of each element as well.
  /// </summary>
  public static bool EqualsUnordered<T>(this IEnumerable<T> first, IEnumerable<T> second) {
    if (first == null)
      return second == null; // or throw if that's more appropriate to your use.
    if (second == null)
      return false;   // likewise.
    var dict = new Dictionary<T, int>(); // You could provide a IEqualityComparer<T> here if desired.
    foreach (T element in first) {
      dict.TryGetValue(element, out int count);
      dict[element] = count + 1;
    }
    foreach (T element in second) {
      if (!dict.TryGetValue(element, out int count))
        return false;
      else if (--count == 0)
        dict.Remove(element);
      else
        dict[element] = count;
    }
    return dict.Count == 0;
  }

  /// <summary>
  /// clone the list of items
  /// </summary>
  public static IEnumerable<T> DeepClone<T>(this IEnumerable<T> listToClone) where T : ICloneable {
    return listToClone.Select(item => (T)item.Clone());
  }

  /// <summary>
  /// clone the list of items
  /// </summary>
  public static IEnumerable<T> Clone<T>(this IEnumerable<T> listToClone) {
    return listToClone.Select(item => item);
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> AsEnumerable<T>(this T obj) {
    return Enumerable.Repeat<T>(obj, 1);
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> ToEnumerable<T>(this Tuple<T,T> tuple) {
    return new T[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> ToEnumerable<T>(this Tuple<T, T, T> tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3};
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> ToEnumerable<T>(this Tuple<T, T, T, T> tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3 , tuple.Item4};
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<object> ToEnumerable(this Tuple<object, object> tuple) {
    return new object[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<object> ToEnumerable(this Tuple<object, object, object> tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3};
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<object> ToEnumerable(this Tuple<object, object, object, object> tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3 , tuple.Item4};
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> ToEnumerable<T>(this (T, T) tuple) {
    return new T[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> ToEnumerable<T>(this (T, T, T) tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<T> ToEnumerable<T>(this (T, T, T, T) tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<object> ToEnumerable(this (object, object) tuple) {
    return new object[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<object> ToEnumerable(this (object, object, object) tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static IEnumerable<object> ToEnumerable(this (object, object, object, object) tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4 };
  }

  /// <summary>
  /// Cartesian tall of these lists together
  /// </summary>
  public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences) {
    if (sequences == null) {
      return null;
    }

    IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>() };

    return sequences.Aggregate(
        emptyProduct,
        (accumulator, sequence) => accumulator.SelectMany(
            accseq => sequence,
            (accseq, item) => accseq.Concat(new[] { item })));
  }

  /// <summary>
  /// Cartesian tall of these lists together
  /// </summary>
  public static IEnumerable Cartesian(this IEnumerable<IEnumerable> items) {
    var slots = items
       // initialize enumerators
       .Select(x => x.GetEnumerator())
       // get only those that could start in case there is an empty collection
       .Where(x => x.MoveNext())
       .ToArray();

    while (true) {
      // yield current values
      yield return slots.Select(x => x.Current);

      // increase enumerators
      foreach (var slot in slots) {
        // reset the slot if it couldn't move next
        if (!slot.MoveNext()) {
          // stop when the last enumerator resets
          if (slot == slots.Last()) { yield break; }
          slot.Reset();
          slot.MoveNext();
          // move to the next enumerator if this reseted
          continue;
        }
        // we could increase the current enumerator without reset so stop here
        break;
      }
    }
  }
}