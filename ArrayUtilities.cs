﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ArrayUtilities {

  public static Return[,] To2DArray<Type, Return>(this IEnumerable<Type> source, Func<Type, (Return value, int x, int y)> mapper) {
    List<List<Return>> values = new List<List<Return>>();
    (int x, int y) extreems = (0, 0);
    foreach(Type entry in source) {
      (Return value, int slotX, int slotY) = mapper(entry);
      if (values.Count() <= slotX) {
        values[slotX] = new List<Return>();
        extreems.y = slotX;
      } else if (values[slotX] == null) {
        values[slotX] = new List<Return>();
      }
      if (values.Count() <= slotY && slotY > extreems.y) {
        extreems.y = slotY;
      }
      values[slotX][slotY] = value;
    }

    var @return = new Return[extreems.x, extreems.y];
    int indexX = 0;
    foreach(var row in values) {
      int indexY = 0;
      foreach (var value in row) {
        @return[indexX, indexY] = value;
      }
    }

    return @return;
  }

  public static Return[,] To2DArray<Type,Return>(this IEnumerable<Type> source, Func<Type, int, (Return value, int x, int y)> mapper) {
    List<List<Return>> values = new List<List<Return>>();
    (int x, int y) extreems = (0, 0);
    int index = 0;
    foreach(Type entry in source) {
      (Return value, int slotX, int slotY) = mapper(entry, index++);
      if (values.Count() <= slotX) {
        values[slotX] = new List<Return>();
        extreems.y = slotX;
      } else if (values[slotX] == null) {
        values[slotX] = new List<Return>();
      }
      if (values.Count() <= slotY && slotY > extreems.y) {
        extreems.y = slotY;
      }
      values[slotX][slotY] = value;
    }

    var @return = new Return[extreems.x, extreems.y];
    int indexX = 0;
    foreach(var row in values) {
      int indexY = 0;
      foreach (var value in row) {
        @return[indexX, indexY] = value;
      }
    }

    return @return;
  }

  /// <summary>
  /// Turn any object into an array
  /// </summary>
  public static T[] AsArray<T>(this T obj) {
    return new T[] { obj };
  }

  /// <summary>
  /// Add values to the end and return the whole new array to chain
  /// </summary>
  public static object[] Prepend(this object[] array, params object[] values) {
    int numberOfValuesToAppend = values.Length;
    object[] newArray = new object[array.Length + numberOfValuesToAppend];
    int newValuesIndex = 0;
    foreach(object newItem in values) {
      newArray[newValuesIndex++] = newItem;
    }
    int currentValuesIndex = 0;
    foreach (object exitingValue in array) {
      newArray[numberOfValuesToAppend + currentValuesIndex++] = exitingValue;
    }

    return array;
  }

  public static void Populate<T>(this T[,] arr, T value) {
    for (int i = 0; i < arr.Length; i++) {
      for (int j = 0; j < arr.Length; j++) {
        arr[i, j] = value;
      }
    }
  }

  public static void Populate<T>(this T[] arr, T value) {
    for (int i = 0; i < arr.Length; i++) {
      arr[i] = value;
    }
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static T[] ToArray<T>(this Tuple<T, T> tuple) {
    return new T[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static T[] ToArray<T>(this Tuple<T, T, T> tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static T[] ToArray<T>(this Tuple<T, T, T, T> tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static object[] ToArray(this Tuple<object, object> tuple) {
    return new object[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static object[] ToArray(this Tuple<object, object, object> tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static object[] ToArray(this Tuple<object, object, object, object> tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static T[] ToArray<T>(this (T, T) tuple) {
    return new T[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static T[] ToArray<T>(this (T, T, T) tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static T[] ToArray<T>(this (T, T, T, T) tuple) {
    return new T[] { tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static object[] ToArray(this (object, object) tuple) {
    return new object[] { tuple.Item1, tuple.Item2 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static object[] ToArray(this (object, object, object) tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3 };
  }

  /// <summary>
  /// any object as en enumberable
  /// </summary>
  public static object[] ToArray(this (object, object, object, object) tuple) {
    return new object[] { tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4 };
  }

  /// <summary>
  /// spread opperators
  /// </summary>
  public static void Deconstruct<T>(this T[] array, out T first, out IList<T> rest) {
    first = array.Length > 0 ? array[0] : default(T);
    rest = array.Skip(1).ToList();
  }

  /// <summary>
  /// spread opperators
  /// </summary>
  public static void Deconstruct<T>(this T[] array, out T first, out T second, out IList<T> rest) {
    first = array.Length > 0 ? array[0] : default(T);
    second = array.Length > 1 ? array[1] : default(T);
    rest = array.Skip(2).ToList();
  }
}