﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Collections {

  /// <summary>
  /// A queue that can remove items by matching like a list
  /// </summary>
  public class QueuedCollection<T> : ICollection<T> {

    uint _currentKey
      = 0;

    Dictionary<uint, T> _items
      = new Dictionary<uint, T>();

    Queue<uint> _keys
      = new Queue<uint>();

    public int Count
      => _items.Count;

    public bool IsReadOnly
      => false;

    /// <summary>
    /// Make a list queue from a collection
    /// </summary>
    public QueuedCollection(IEnumerable<T> items = null) {
      if (items != null) {
        items.ForEach(Enqueue);
      }
    }

    /// <summary>
    /// Remove the first item from the queue.
    /// Throws if there's no items in the queue.
    /// </summary>
    public T Dequeue() {
      bool validKeyFound = false;
      T value = default;
      uint currentItemKey = 0;

      while (!validKeyFound) {
        currentItemKey = _keys.Dequeue();
        if (_items.TryGetValue(currentItemKey, out value)) {
          validKeyFound = true;
        }
      }

      _items.Remove(currentItemKey);
      return value;
    }

    /// <summary>
    /// Try to remove the first item from the queue
    /// </summary>
    public bool TryToDequeue(out T value) {
      bool validKeyFound = false;
      uint currentItemKey = 0;
      value = default;

      while (!validKeyFound) {
        if (Count == 0) {
          return false;
        }
        currentItemKey = _keys.Dequeue();
        if (_items.TryGetValue(currentItemKey, out value)) {
          validKeyFound = true;
        }
      }

      _items.Remove(currentItemKey);
      return true;
    }

    /// <summary>
    /// Enqueue an item
    /// </summary>
    /// <param name="item"></param>
    public void Enqueue(T item)
      => Add(item);

    /// <summary>
    /// Enqueue an item
    /// </summary>
    /// <param name="item"></param>
    public void Add(T item) {
      uint itemKey = getNextKey();
      _items.Add(itemKey, item);
      _keys.Enqueue(itemKey);
    }

    public void Clear() {
      _currentKey = 0;
      _items.Clear();
      _keys.Clear();
    }

    public bool Contains(T item)
      => _items.ContainsValue(item);

    public void CopyTo(T[] array, int arrayIndex)
     => _items.Values.CopyTo(array, arrayIndex);

    /// <summary>
    /// Remove all instances that match the given item from the queue
    /// </summary>
    public bool Remove(T item) {
      bool found = false;
      if (_items.ContainsValue(item)) {
        foreach (KeyValuePair<uint, T> entry in _items.Where(kvp => kvp.Value.Equals(item)).ToList()) {
          _items.Remove(entry.Key);
          found = true;
        }
      }

      return found;
    }

    /// <summary>
    /// Remove all instances that match the given item from the queue
    /// </summary>
    public bool Remove(T item, out T found) {
      if (_items.ContainsValue(item)) {
        foreach (KeyValuePair<uint, T> entry in _items.Where(kvp => kvp.Value.Equals(item)).ToList()) {
          found = entry.Value;
          _items.Remove(entry.Key);
          return true;
        }
      }

      found = default;
      return false;
    }

    /// <summary>
    /// Remove all instances that match the given item from the queue
    /// </summary>
    public IEnumerable<T> RemoveEach(T item) {
      if (_items.ContainsValue(item)) {
        foreach (KeyValuePair<uint, T> entry in _items.Where(kvp => kvp.Value.Equals(item)).ToList()) {
          T foundItem = _items[entry.Key];
          _items.Remove(entry.Key);
          yield return foundItem;
        }
      }
    }

    /// <summary>
    /// Remove all instances that match the given logic
    /// </summary>
    public bool RemoveAll(Func<T, bool> shouldRemove) {
      bool found = false;
      foreach (KeyValuePair<uint, T> entry in _items.Where(kvp => shouldRemove(kvp.Value)).ToList()) {
        _items.Remove(entry.Key);
        found = true;
      }

      return found;
    }

    /// <summary>
    /// Remove all instances that match the given logic until a condition is met (null return)
    /// </summary>
    public IEnumerable<T> RemoveEach(Func<T, bool?> shouldRemoveUntil) {
      foreach (KeyValuePair<uint, T> entry in _items.Where(kvp => shouldRemoveUntil(kvp.Value)).ToList()) {
        T foundItem = _items[entry.Key];
        _items.Remove(entry.Key);
        yield return foundItem;
      }
    }

    public IEnumerator<T> GetEnumerator()
      => _keys.SelectWhere(
        key => _items.TryGetValue(key, out T value)
        ? (true, value)
        : (false, default)
      ).GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
      => GetEnumerator();

    uint getNextKey() {
      if (_currentKey == uint.MaxValue) {
        _currentKey = 0;
      }

      return _currentKey++;
    }
  }

  public static class ListQueueExtensions {

    public static QueuedCollection<T> ToListQueue<T>(this IEnumerable<T> enumerable)
      => new QueuedCollection<T>(enumerable);
  }
}
